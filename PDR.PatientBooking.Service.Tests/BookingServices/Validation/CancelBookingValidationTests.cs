﻿using System;
using AutoFixture;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using PDR.PatientBooking.Data;
using PDR.PatientBooking.Service.BookingServices.Requests;
using PDR.PatientBooking.Service.BookingServices.Validation;

namespace PDR.PatientBooking.Service.Tests.BookingServices.Validation
{
    public class CancelBookingValidationTests
    {
        [TestFixture]
        public class AddClinicRequestValidatorTests
        {
            private IFixture _fixture;

            private PatientBookingContext _context;

            private CancelBookingRequestValidator _validator;

            [SetUp]
            public void SetUp()
            {
                // Boilerplate
                _fixture = new Fixture();

                //Prevent fixture from generating from entity circular references 
                _fixture.Behaviors.Add(new OmitOnRecursionBehavior(1));

                // Mock setup
                _context = new PatientBookingContext(new DbContextOptionsBuilder<PatientBookingContext>()
                    .UseInMemoryDatabase(Guid.NewGuid().ToString()).Options);

                // Mock default
                SetupMockDefaults();

                // Sut instantiation
                _validator = new CancelBookingRequestValidator(
                    _context
                );
            }

            private void SetupMockDefaults()
            {
               
            }

            // place a test here with a passing test that has a valid booking in to cancel
           
            [Test]
            public void ValidateRequest_BookingNotExists_ReturnsFailedValidationResult()
            {
                //arrange
                var request = GetValidRequest();
                request.DoctorId = 2;
                request.PatientId = 159;
                request.StartTime = new DateTime(2021, 4, 12);

                //act
                var res = _validator.ValidateRequest(request);

                //assert
                res.PassedValidation.Should().BeFalse();
                res.Errors.Should().Contain("This booking could not be found.");
            }

            private CancelBookingRequest GetValidRequest()
            {
                var request = _fixture.Create<CancelBookingRequest>();
                return request;
            }
        }
    }
}
