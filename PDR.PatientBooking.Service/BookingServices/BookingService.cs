﻿using System;
using System.Linq;
using PDR.PatientBooking.Data;
using PDR.PatientBooking.Service.BookingServices.Requests;
using PDR.PatientBooking.Service.BookingServices.Validation;

namespace PDR.PatientBooking.Service.BookingServices
{
    public class BookingService : IBookingService
    {
        private readonly PatientBookingContext _context;
        private readonly ICancelBookingRequestValidator _validator;

        public BookingService(PatientBookingContext context, ICancelBookingRequestValidator validator)
        {
            _context = context;
            _validator = validator;
        }

        public void CancelBooking(CancelBookingRequest request)
        {
            var validationResult = _validator.ValidateRequest(request);

            if (!validationResult.PassedValidation)
            {
                throw new ArgumentException(validationResult.Errors.First());
            }

            var booking = _context.Order.FirstOrDefault(x =>
                    x.PatientId == request.PatientId &&
                    x.StartTime == request.StartTime &&
                    x.DoctorId == request.DoctorId);

            // it won't be null because the validation will have caught it above if so
            booking.IsCancelled = true;

            _context.Order.Update(booking);

            _context.SaveChanges();
        }
    }
}
