﻿using PDR.PatientBooking.Service.BookingServices.Requests;
using System.Linq;
using PDR.PatientBooking.Data;
using PDR.PatientBooking.Service.Validation;

namespace PDR.PatientBooking.Service.BookingServices.Validation
{
    public class CancelBookingRequestValidator : ICancelBookingRequestValidator
    {
        private readonly PatientBookingContext _context;

        public CancelBookingRequestValidator(PatientBookingContext context)
        {
            _context = context;
        }

        public PdrValidationResult ValidateRequest(CancelBookingRequest request)
        {
            var result = new PdrValidationResult(true);

            //if (DoctorIsBusy(request, ref result))
            //    return result;

            //if (BookingIsRequestedForAPastDate(request, ref result))
            //    return result;

            if (BookingNotFound(request, ref result))
                return result;

            return result;
        }

        //private bool BookingIsRequestedForAPastDate(CancelBookingRequest request, ref PdrValidationResult result)
        //{
        //    //    // put logic here from controller
        //}

        //private bool DoctorIsBusy(CancelBookingRequest request, ref PdrValidationResult result)
        //{
        //    // put logic here from controller
        //}

        private bool BookingNotFound(CancelBookingRequest request, ref PdrValidationResult result)
        {
            if (!_context.Order.Any(x =>
                x.PatientId == request.PatientId &&
                x.StartTime == request.StartTime &&
                x.DoctorId == request.DoctorId))
            {
                result.PassedValidation = false;
                result.Errors.Add("This booking could not be found.");
                return true;
            }

            return false;
        }
    }
}
