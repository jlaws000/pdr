﻿using System;

namespace PDR.PatientBooking.Service.BookingServices.Requests
{
    public class CancelBookingRequest
    {
        public DateTime StartTime { get; set; }
        public long PatientId { get; set; }
        public long DoctorId { get; set; }
    }
}
